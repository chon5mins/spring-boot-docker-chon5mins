package com.chon5mins.springbootdockerchon5mins;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootDockerChon5minsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootDockerChon5minsApplication.class, args);
	}

}
