package com.chon5mins.springbootdockerchon5mins.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HelloDto {
    private String message;
}
