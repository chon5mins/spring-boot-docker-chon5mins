package com.chon5mins.springbootdockerchon5mins.controller;

import com.chon5mins.springbootdockerchon5mins.dto.HelloDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class HelloController {
    @GetMapping("/hello")
    public HelloDto hello(){
        HelloDto helloDto = new HelloDto();
        helloDto.setMessage("Hello from home!");

        return helloDto;
    }
}
