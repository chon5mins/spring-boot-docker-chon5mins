FROM openjdk:8-jdk-alpine
EXPOSE 8080
ADD ./target/spring-boot-docker-chon5mins-1.jar app.jar
ENTRYPOINT ["java","-jar","app.jar"]